<?php

//$parse = parse_url( $_SERVER['REQUEST_URI']);
//print_r($parse);
//exit;
//$url = explode('/', $_SERVER['REQUEST_URI']);
//$new_url = array_diff($url, array(''));
//print_r(array_values($new_url));
//$params = array_slice($url, 3);
//print_r($params);
//exit;
require_once("bootstrap.php");

class  Index
{

    public $config = array();
    public $request;
    public $controller = "home";
    public $method = "index";
    public $param;
    public $class;

    public function  __construct()
    {

        $this->request = $_SERVER['REQUEST_URI'];
        $this->loadController();
        $param = $this->getParam();


        $count_param = count($param);
        $arg_1 = "";
        $arg_2 = "";
        if ($count_param == 0) {
            call_user_func(array($this->getController(), $this->getMethod()));
        } elseif ($count_param == 1) {

            $arg_1 = $param[0];

            call_user_func_array(array($this->getController(), $this->getMethod()), array($arg_1));

        } elseif (($count_param == 2)) {
            $arg_1 = $param[0];
            $arg_2 = $param[1];
            call_user_func_array(array($this->getController(), $this->getMethod()), array($arg_1, $arg_2));
        }


    }

    public function getParamUri()
    {

        $url = explode('/', $this->request);

        $new_url = array_diff($url, array(''));

        return array_values($new_url);
    }

    public function getController()
    {

        $controller = $this->getParamUri();

        if (array_key_exists(0, $controller)) {
            return $this->controller = $controller[0];
        } else {

            return $this->controller = "test";

        }


    }

    public function getMethod()
    {
        $method = $this->getParamUri();

        if (array_key_exists(1, $method)) {
            return $this->method = $method[1];
        } else {

            return $this->method = "index";

        }

    }

    public function getParam()
    {

        $param = $this->getParamUri();
        $params = array_slice($param, 2);

        $this->param = $params;
        return $this->param;

    }

    public function loadController()
    {

        include_once(APPPATH . 'controllers/' . $this->getController() . '.php');

        $this->class = new $this->controller;


    }

}

$test = new Index();

