<?php

class Model {

    public function __construct() {


        $this->autoload();

    }


    protected function  autoload()
    {

        $dir = APPPATH.'/model/';
        $new_model = array();
        $files = scandir($dir);
        $model = array_diff($files, array('.','..'));
        $new_model = array_values($model);

        for ($i=0;$i<count($new_model);$i++) {

            $file =   APPPATH.'model/'.$new_model[$i];

            if (file_exists($file) == false)
            {
                return false;
            }

            include_once ($file);

        }

    }

    public function load($name) {
        include (APPPATH.'model/'.$name.'.php');

    }


}
