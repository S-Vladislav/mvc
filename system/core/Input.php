<?php

class Input {

    function  __construct() {

    }

    public static function get($index = NULL)
    {
        if ($index === NULL AND ! empty($_GET))
        {
            $get = array();

            foreach (array_keys($_GET) as $key)
            {
                $get[$key] = Input::fetch_from_array($_GET, $key);
            }
            return $get;
        }

        return Input::fetch_from_array($_GET, $index);
    }

    public static  function post($index = NULL)
    {
        if ($index === NULL AND ! empty($_POST))
        {
            $post = array();

            foreach (array_keys($_POST) as $key)
            {
                $post[$key] = Input::fetch_from_array($_POST, $key);
            }
            return $post;
        }

       return  Input::fetch_from_array($_POST, $index);
    }

    public static  function fetch_from_array(&$array, $index = '')
    {
        if ( ! isset($array[$index]))
        {
            return FALSE;
        }


        return $array[$index];
    }

}