<?php


class View {


    static  function load($name,$data=NULL) {

        if(is_array($data)) {

            extract($data);
        }

        include  (APPPATH.'views/'.$name.'.php');

    }


}
