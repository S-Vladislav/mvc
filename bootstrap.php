<?php

ini_set('display_errors', 1);

$application_folder = 'app';

$system_path = 'system';

if (is_dir($application_folder))
{
    define('APPPATH', $application_folder.'/');
}

if (realpath($system_path) !== FALSE)
{
    $system_path = realpath($system_path).'/';
}

$system_path = rtrim($system_path, '/').'/';

define('BASEPATH', str_replace("\\", "/", $system_path));

function __autoload($class_name)
{
    $filename = $class_name.'.php';
 $file =   BASEPATH.'core/'.$filename;

    if (file_exists($file) == false)
    {
        return false;
    }

    include_once ($file);

  $class_name = new $class_name;
}

function __autoloadhelper($helper_name)
{
    $filename = "helper_".$helper_name.'.php';
    $file =   APPPATH.'helpers/'.$filename;

    if (file_exists($file) == false)
    {
        return false;
    }

    include_once  ($file);

}

__autoload("Controller");
__autoload("Model");
__autoload("Database");
__autoload("Input");
__autoload("View");
__autoloadhelper("script");
__autoloadhelper("config");

require_once BASEPATH.'core/Init.php';