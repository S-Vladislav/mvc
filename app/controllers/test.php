<?php

class test extends Controller
{

    public function getTest()
    {
        $main = new main_model();

        $tests = $main->getTest();

        $response = array('response' => array("items" => $tests));

        echo json_encode($response);


    }

    public function addTest()
    {

        $check = false;

        $main = new main_model();

        $name = Input::get("name");
        $theme = Input::get("theme");

        $valid_name = $main->check_test($name);

        if (!empty($name) && !empty($theme) && $valid_name == true) {
            $check = $main->add_test($name, $theme);
        }

        if ($check) {
            $response['message'] = "success";
        } else {
            $response['message'] = "error";
        }


        echo json_encode($response, JSON_FORCE_OBJECT);

    }

    public function getTestByName()
    {

        $main = new main_model();

        $name = Input::get("name");

        $data= $main->get_test_by_name($name);

        echo json_encode($data, JSON_FORCE_OBJECT);

    }

    public function addQuestion()
    {

        $check = false;

        $main = new main_model();

        $id_test = Input::get("id_test");

        $answer = Input::get();

        unset($answer['id_test']);

        $count_answer = count(Input::get());

        if ($count_answer > 1 &&  $id_test != 0) {

            $check = $main->add_question($answer, $id_test);
        } else {
            $response['message'] = "error";
        }
        if ($check) {
            $response['message'] = "success";
        } else {
            $response['message'] = "error";
        }

        echo json_encode($response, JSON_FORCE_OBJECT);

    }

    public function deleteTest()
    {
        $main = new main_model();
        $id_test = Input::get("id_test");
        $response = $main->delete_test($id_test);
        echo json_encode($response);

    }

    public function deleteQuestion()
    {
        $main = new main_model();
        $id_question = Input::get("id_question");
        $response = $main->delete_question($id_question);
        echo json_encode($response);


    }
    public function updateQuestion()
    {
        $main = new main_model();
        $id_question = Input::get('id_question');
        $id_test  = Input::get('id_test');
        $question_text = Input::get('question_text');
        $answer_text_one = Input::get('answer_text_one');
        $answer_text_two = Input::get('answer_text_two');
        $answer_text_three = Input::get('answer_text_three');
        $answer_true = Input::get('answer_true');

       if ($main->update_question($id_question,$question_text,$answer_text_one,$answer_text_two,$answer_text_three,$answer_true));
        echo "ok";
    }

    public function getQuestionById() {

        $main = new main_model();

        $id = Input::get('id_question');

        $question  = $main->get_question_by_id($id);

        $response = array('response' => array("items" => $question));

        echo json_encode($response);
    }

    public function getAllQuestion() {

        $main = new main_model();
        $question  = $main->get_all_question();

        $response = array('response' => array("items" => $question));

        echo json_encode($response);
    }


    public function updateTest()
    {
        $main = new main_model();

        $id = Input::get("id_test");
        $name  = Input::get("name_test");
        $theme = Input::get("theme_test");

       $response =  $main->update_test($id,$name,$theme);
        echo json_encode($response);
    }

    public function getAllTest()
    {
        $main = new main_model();
        $tests = $main->get_test_question();
        $response = array('response' => array("items" => $tests));
        echo json_encode($response);
    }

    public function getTestById()
    {

        $main = new main_model();
        $id = Input::get("id_test");
        $data = $main->get_test_by_id_count_question($id);
        $response = array('response' => array("items" => $data));
        echo json_encode($response);


    }
}