<?php

class main_model extends Model
{

    public $db;

    public function __construct() {

        $this->db = DataBase::getDB();

    }

    public function getTest() {

        $sql = "SELECT `id` , `name`, `theme` FROM `test`";
        $test = $this->db->select($sql);
        return $test;
    }

    public function add_test($name,$theme) {

        $query =  "INSERT INTO `test` (`name`, `theme`) VALUES ('".$name."','".$theme."')";

        return $this->db->query($query);

    }

    public function get_test_by_name($name) {
        $query = "SELECT `id` FROM `test` WHERE `name` = {?}";
        $test_name = $this->db->selectRow($query, array($name));
        return $test_name;

    }
    public function get_test_by_id($id) {
        $query = "SELECT `id`, `name` ,`theme` FROM `test` WHERE `id` = {?}";
        $test = $this->db->select($query, array($id));
        return $test;

    }
    public function get_test_by_id_count_question($id) {
        $query = "SELECT COUNT(*) Total_question, `test`.`id`, `test`.`name`, `test`.`theme`, `question`.`id_test` FROM `test` INNER JOIN `question` ON `test`.`id` = `question`.`id_test` WHERE `question`.`id_test` = {?}";
        $test = $this->db->select($query, array($id));
        return $test;

    }
    public function add_question($data,$id_test) {

        $check  = false;
        $count = 5;
        $i = 0;
        foreach ($data as $a => $value) {
            if (!empty($value)){
            $i++;
            $query =  "INSERT INTO `question` ( `id_test`, `question_text`,`answer_text_one`,`answer_text_two`,`answer_text_three`, `answer_true`) VALUES ('".$id_test."' ,'".$data['question_'.$i]."','".$data['answer_1_'.$i]."','".$data['answer_2_'.$i]."','".$data['answer_3_'.$i]."','".$data['answer_true_'.$i]."')";

            $check = $this->db->query($query);


            if($count <=0)
                break;

            $count--;
            }

        }
        return $check;

    }

    public function check_test($name){

        $query = "SELECT `id` FROM `test` WHERE `name` = {?}";
        $test_check = $this->db->selectCell($query, array($name));

        if (count($test_check) > 0) {

            return false;

        } else {

            return true;
        }

    }
    public function get_test_question() {

        $query =  "SELECT `test`.`name` , `test`.`id` ,  `question`.`question_text`, `question`.`answer_text` FROM `test` INNER JOIN `question` ON `test`.`id` = `question`.`id_test`";

        return $this->db->select($query);

    }


    public function delete_test($id_test) {

        $query = "DELETE FROM `test` WHERE `id` = {?}";
        return $this->db->query($query,array($id_test));

    }

    public function update_test($id,$name,$theme) {


        $query  = "UPDATE `test` SET `name` = {?},`theme`={?} WHERE `id` = {?}";

        return $this->db->query($query,array($name,$theme,$id));
    }

    public function get_question_by_id($id) {

        $query = "SELECT * FROM `question` WHERE `id` = {?}";
        $question = $this->db->select($query, array($id));
        return $question;

    }

    public function get_all_question() {

        $query = "SELECT  *  FROM `question`";
        $question = $this->db->select($query);
        return $question;

    }

    public function delete_question($id_question) {

        $query = "DELETE FROM `question` WHERE `id` = {?}";
        return $this->db->query($query,array($id_question));

    }

    public function update_question($id_question,$question_text,$answer_text_one,$answer_text_two,$answer_text_three,$answer_true) {

        $query = "UPDATE `question` SET `question_text`={?},`answer_text_one`={?},`answer_text_two`={?},`answer_text_three`={?},`answer_true`={?} WHERE `id`={?}";
        return $this->db->query($query,array($question_text,$answer_text_one,$answer_text_two,$answer_text_three,$answer_true,$id_question));

    }

}