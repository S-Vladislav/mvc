<?php

function config_get($field) {

    $filename = "config.php";
    $file =   APPPATH.'config/'.$filename;

    if (file_exists($file))
    {
        include  ($file);

        return $field ? $config[$field] : $config;


    }

}